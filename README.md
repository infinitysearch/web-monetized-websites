# Web Monetized Websites

This repository holds a list of sites and webpages that are using the web monetization standard. The [web_monetized_sites_homepages.txt file](/web_monetized_sites_homepages.txt) is a list of homepages with each site on a new line. The [web_monetized_pages.json file](/web_monetized_pages.json) is a JSON list of webpages with more information about each page provided, such as their title, description, payment pointer, if that page uses javascript, and more.

We are building an index of these sites and their contents on Infinity Search at [https://infinitysearch.co/results/web_monetized?q=](https://infinitysearch.co/results/web_monetized?q=). 

